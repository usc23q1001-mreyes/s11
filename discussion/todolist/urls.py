from django.urls import path, re_path
from . import views


# Syntax
# path(route, view, name)

app_name ='todolist'

urlpatterns = [
	path('', views.index, name='index'),
	#/todolist/<todoitem_id>
	#/todolist/1
	# /todolist/register
	path('register', views.register, name='register'),
	path('change_password', views.change_password, name="change_password"),
	path('login', views.login_view, name="login"),
	path('logout', views.logout_view, name="logout"),
	path('add_task', views.add_task, name="add_task"),
	path('add_event', views.add_event, name="add_event"),
    # The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
	# path('<int:eventitem_id>/', views.eventitem, name='vieweventitem'),
	# path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
	re_path(r'^eventitem/(?P<eventitem_id>\d+)/$', views.eventitem, name='vieweventitem'),
    re_path(r'^todoitem/(?P<todoitem_id>\d+)/$', views.todoitem, name='viewtodoitem'),
	path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
	path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
	
]