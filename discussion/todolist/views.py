from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, EventItem

# Local Imports
from .models import ToDoItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateProfileForm


def index(request):
	
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	eventitem_list = EventItem.objects.filter(user_id=request.user.id)
	context = {
		'todoitem_list': todoitem_list,
		'eventitem_list': eventitem_list,
		'user': request.user
		}

	return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
	
	# todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):
	context = {}

	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid() == False:
			form = RegisterForm()
		else:
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']

			duplicate = User.objects.filter(username=username)

			if duplicate:
				context = {
					"error": True,
					"message": "username already exists"
				}
			elif password != confirm_password:
				context = {
					"error": True,
					"message": "password not matching"
				}
			else:
				user = User()
				user.username = username
				user.first_name = first_name
				user.last_name = last_name
				user.email = email
				user.set_password(password)
				user.is_staff = False
				user.is_active = True
				user.save()
				
				user = authenticate(username=username, password=password)
				login(request, user)
				return redirect('todolist:index')

	return render(request, "todolist/register.html", context)


def change_password(request):
	user = request.user
	context = {
		'first_name': user.first_name,
		'last_name': user.last_name
	}

	if request.method == 'POST':
		form = UpdateProfileForm(request.POST)
		if form.is_valid() == False:
			form = UpdateProfileForm()
		else:
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			password = form.cleaned_data['password']
			user.set_password(password)
			user.first_name = first_name
			user.last_name = last_name
			user.save()
			context = {
				'success': True,
				'message': 'Successfully Updated Profile'
			}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	context = {}

	# If this is a POST request we need to process the form data
	if request.method == 'POST':
		# Create a form instance and populate it with data from the request
		form = LoginForm(request.POST)

		# Check whether the data is valid
		# Runs validation routines for all the form fields and returns True and places the form's data in the "cleaned_data" attribute
		if form.is_valid() == False:
			# Returns a blank login form
			form = LoginForm()
		else:
			# Retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username": username,
				"password": password
			}

			if user is not None:
				#Saves the user's ID in the session using Django's session framework
				login(request, user)
				return redirect('todolist:index')
			else:
				# Provides context with error to conditionally render the error message
				context = {
					"error": True
				}
	
	return render(request, "todolist/login.html", context)


def logout_view(request):
	logout(request)
	return redirect("todolist:index")



def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)
		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			# Checks the database if a task already exists
			# By default the "filter" method searches for records that are case insensitive
			duplicates = ToDoItem.objects.filter(task_name=task_name)

			# if todoitem does not contain any duplicates
			if not duplicates:
				# Creates an object based on the "TodoItem" model and saves the record in the database
				ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
				return redirect('todolist:index')
			
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):
	# Returns a queryset
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	# Accessing the first element is necessary because the 
	# "ToDoItem.objects.filter()" method returns a queryset
	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status": todoitem[0].status,
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)
		if form.is_valid() == False:
			form = UpdateTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()
				return redirect('todolist:index')
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect('todolist:index')



def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			print('here error')
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicate = EventItem.objects.filter(event_name=event_name)

			if not duplicate:
				EventItem.objects.create(event_name=event_name, description=description, user_id=request.user.id)
				return redirect('todolist:index')
			else:
				context = {
					"error": True,
					"message": 'event already created'
				}

	return render(request, "todolist/add_event.html", context)


def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(EventItem, pk=eventitem_id)
	return render(request, "todolist/eventitem.html", model_to_dict(eventitem))